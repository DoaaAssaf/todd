#!/bin/sh

# start todd server
nohup gradle runServerRemote -p /home/gradle/todd > /dev/null 2>&1 &

# restart passive monitoring agent
pkill -f nagiosPassive
sleep 3 # wait a bit for todd to launch
nohup gradle nagiosPassive -p /home/gradle/todd > /dev/null 2>&1 &
